public class Employee {
    int id;
    String firstName;
    String lastName;
    int salary;
    public Employee(){
        super();
    }
    // constructors
    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }
    // getter setter
    public int getId() {
        return id;
    }
    

    public void setId(int id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    public String name() {
        return this.firstName + this.lastName;
    }
    // tiền lương 1 năm
    public int getAnnualSalary(){
        return (this.salary)*12;
    }
    // lương sau khi được tăng
    public float raiseSalary(float percent){
        return ((percent/100)+1)*(this.salary);
    }
    @Override
    public String toString() {
        return "Employee [firstName=" + firstName + ", id=" + id + ", lastName=" + lastName + ", salary=" + salary
        + "]";
    }
    
    
}
